app.controller('HomeController', [
        "$scope",
        function ($scope) {
            
        $scope.carsArray = [{
            url: '../images/mitsubishi1.jpg',
            nev: 'Mitsubishi',
            tipus: 'Carisma',
            evjarat: '1997',
            ar: '750.000 Ft',
            type: 1
          }, {
            url: '../images/subaru1.jpg',
            nev: 'Subaru',
            tipus: 'WRX',
            evjarat: '2010',
            ar: '2.450.000 Ft', 
            type: 2
          }, {
            url: '../images/volvo1.jpg',
            nev: 'Volvo',
            tipus: 'P130 AMAZON 2-D',
            evjarat: '1970',
            ar: '8.750.000 Ft', 
            type: 5
          }, {
            url: '../images/mitsubishi2.jpg',
            nev: 'Mitsubishi',
            tipus: 'Lancer',
            evjarat: '2000',
            ar: '1.000.000 Ft', 
            type: 1
        }, {
            url: '../images/ford1.jpg',
            nev: 'Ford',
            tipus: 'Mustang',
            evjarat: '1957',
            ar: '10.000.000 Ft', 
            type: 3
        }, {
            url: '../images/volkswagen1.jpg',
            nev: 'Volkswagen',
            tipus: 'Golf',
            evjarat: '1981',
            ar: '200.000 Ft', 
            type: 4
        }, {
            url: '../images/mitsubishi2.jpg',
            nev: 'Mitsubishi',
            tipus: 'Lancer',
            evjarat: '2008',
            ar: '2.200.000 Ft', 
            type: 1
        }];
        
        $scope.calculateSideNavHeight = function () {
            var fdSideNav = $('.fd-sidenav');
            fdSideNav.css('height', $(document).height() - 56 + "px");
        }
    
        $scope.opendropDown = function () {
            var dropDownContentHeight = $('.fd-dropdown-content').children().innerHeight();
            var dropDownContentLength = $('.fd-dropdown-content a').length + 1;
            var dropDownContentActualheight = dropDownContentHeight * dropDownContentLength;

            if ( $('.fd-dropdown-menu-btn').attr('data-click') == 1 ) {
                $('.fd-dropdown-menu-btn').attr('data-click', 2);
                $('.fd-dropdown-menu').css('height', dropDownContentActualheight + 'px' );
                $('.fd-dropdown-menu-btn i').addClass('active');
            } else {
                $('.fd-dropdown-menu-btn').attr('data-click', 1);
                $('.fd-dropdown-menu').css('height', '35px' );
                $('.fd-dropdown-menu-btn i').removeClass('active');
            }
        }

        $scope.backToTheTop = function () {
            $(window).scroll(function () {
                var windowActualHeight = $(window).scrollTop();
                var showPoint = $('.products-container').innerHeight() / 2;
                if ( windowActualHeight >= showPoint ) {
                    $('.back-to-the-top').addClass('showButton');
                } else {
                    $('.back-to-the-top').removeClass('showButton');
                }
            });
            $('.back-to-the-top').on('click', function () {
                $("html, body").animate({scrollTop: 0}, 1000);
            });
        }

        angular.element(document).ready(function () {
            $scope.calculateSideNavHeight();
            $scope.backToTheTop();
        });

        }
    ]);